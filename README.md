Заглушка.

Заглушка выполняет следующее:

1.  в заглушку приходит json 
{
    "name": "Vadim",
    "surName": "Ivanov",
    "salary": 52
} 
в виде POST запроса (URL http://localhost:8080/Person).
JSON поля со следующими типами java name - String, surName - String, salary - int.


2. json парсится в джава объект Person;
3. генерируется json объект ответа на запрос вида:

{
    "name": "Vadim",
    "surName": "Ivanov",
    "salary": 52,
    "allSalary": 686
}
где поле allSalary - ЗП сотрудника (Person) за год с учётом премии 10% от оклада.


4. Для запуска программы:
   4.1 Скачать репозиторий git clone git@gitlab.com:Ptitov/simplejson.git (SSH).
   4.2 Перейти в директорию скачанного репо.
   4.3 В командной строке выполнить команду java -jar demo.jar, где demo.jar название jar файла из репо.
   4.4 Для проверки работы можно отправить JSON (см пункт 1) в виде POST запроса (URL http://localhost:8080/Person).
   
ЗЫ: На корректность данных проверку не делал поэтому отобразится и отрицательная ЗП и нулевая.   
